---
title: "{{ replace .Name "-" " " | title }}"
description: ""
date: {{ .Date }}
link: ''
layout: 'portfolio'
featured: false
screenshot: "{{ replace .Name "-" " " | title }}.png"
--- 
