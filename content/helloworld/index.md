---
menu:
  main:
    weight: 3
title: "Hello World!"
---

Ecco un po’ di `hello world` in vari linguaggi di programmazione:


common lisp:
``` common-lisp
(format t "Hello, World!~%")
```
gdscript:
``` gdscript
print("Hello World!")
```
c:
```c
printf("Hello World!\n");
```
bash:
```bash
echo "Hello World!"
```
java:
```JAVA
System.out.println("Hello World!")
```	
crystal:
```crystal
puts "Hello World"
```
C Xlib:

```c xlib
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <X11/Xlib.h>

int
main(void)
{
  Display *d;
  int screen;
  Window root, bar;
  // int width, height;
  Colormap cmap;
  XEvent event;
  GC gc;

  d = XOpenDisplay(NULL);

  if(d == NULL)
    {
      err(1, "Cannot open display");
      exit(1);
    }


  root = DefaultRootWindow(d);
  screen = DefaultScreen(d);
  cmap = DefaultColormap(d, screen);

  bar = XCreateSimpleWindow(d, root,
			      0, 0, 500, 30, 1,
		      BlackPixel(d, screen), WhitePixel(d, screen));
  set_w_atoms(d, bar, 900, 30);

  XStoreName(d, bar, "hello");

  gc = XCreateGC(d, bar, 0, NULL);
  XSetForeground(d, gc, BlackPixel(d, screen));

  XSelectInput(d, bar, ExposureMask | KeyPressMask);

  XMapWindow(d, bar);

  while(1)
    {
      XNextEvent(d, &event);
      if(event.type==Expose && event.xexpose.count <1)
	XDrawString(d, bar, gc, 10, 10, "Hello World!", 12);
      else if(event.type==ButtonPress) break;
    }
```
---

Ora siete in grado di fare debug in alcuni linguaggi di progammazione, senza fare la fatica di andare a cercare in giro :)
