---
weight: 8
title: "HelloGodot!"
date: 2020-12-11
---
## Introduzione
Oramai è un anno che uso Godot Engine per creare giochi con lo scopo di
Divertirmi. In questo anno ho fatto il tutorial base per amici 3-4
volte, mi sono stufato di farlo, quindi ecco qui un tutorial sul come
iniziare e qualche *tips* che spero vi faccia amare questo Engine come
io lo amo.

Non faremo nulla di particolare in questo tutorial, il classico "Hello
World" o "Hello Godot" in questo caso, quello che intendo per *Hello
Godot* è un semplice "Player", un'entità con la quale il giocatore si
possa muovere, ma sopratutto vedremo l'interfaccia grafica di Godot e
cercheremo di prendere familiarità con il mezzo.

## Interfaccia
Partiamo dall'interfaccia di quando si apre Godot la prima volta, si
può notare due **Tab** in alto a sinistra *Project* e *Templates*, per
il momento ignorammo la cosa, a destra invece abbiamo dei menu tra cui
*Edit*, *Run*, *Scan*, *New project*, *Import*, *Rename* e *Remove*. 
Non starò qui a spiegarvi tutti i tasti, iniziamo creando il primo
progetto, cliccando su (strano a dirsi) *New Project*, vi si aprirà
una finestra come nella foto, usate Browse per navigare tra le
cartelle e scegliete dove creare il vostro progetto, dopo di che
scegliete *Create Folder*, cosi facendo avete creato una cartella con
il nome del progetto, poi in fine il tasto *Create & Edit*. 

![](newproject.png)

Ora una volta fatto *Create & Edit* si aprirà una nuova schermata, io
ho trovato questa schermata molto intuitiva la prima volta (perchè
ridi? Non sto scherzando), ma andiamo a guardare e identificare le i
vari *menu*, in alto a sinistra abbiamo il classico menu a tendina con
le varie voci (File, Project, Debug, Editor e Help), Sotto questo menu
abbiamo il riquadro **filesystem** qui potete creare cartelle e
inserire i file che vi serviranno, nulla di complicato. Ora guardiamo
quello evidenziato in Viola, la parte centrale, ci sono tutti gli
strumenti che ti aspetti, ma focalizziamo la nostra attenzione sulla
scritta [Empty], quello significa che ci troviamo in Una Scena vuota,
Se guardiamo la parte grigia sotto vediamo che non è presente nulla
di nulla, quella parte grigia rappresenta la nostra scena, li dove
andremo a lavorare al gioco. Tornando alla scritta [empty X] vicino a
destra è presente il simbolo '+', che vi servirà per creare nuove
scene. 
Continuiamo a studiare l'interfaccia di Godot e guardiamo la zona di
Destra, in alto ci dei tasti avviare le Scene di gioco o la scena
principale e per interrompere. Sotto, evidenziati con il colore Rosso,
ci sono dei *Menu* con due tab per ogni Menu. Focalizziamo la nostra
attenzione sul tab scene, dove vediamo la possibilità di scegliere il
nodo da cui partire, abbiamo 2D Scene, 3D Scene, User Interface e
Other Node. Li possiamo creare il nodo da cui partire e nella quale
iniziare a dare un senso alla scena di gioco. Sotto al tab Scene
esistono due tab che sono *Inspector* e *Node*, ora vi spigo solo
*Inspector* cosi possiamo iniziare il vero tutorial, Inspector è il
luogo dove sarà possibile modificare alcuni aspetti del vostro nodo,
larghezza, posizione, rotazione, e altro ancora, tutto dipende dal tipo
di nodo che abbiamo selezionato. Gli altri menu e tasti ve li
spiegherò in altri tutorial o più in là, ora avete un'idea di come sia
l'interfaccia di Godot e possiamo iniziare con le basi. 

![](interfaccia.png)

## Godot Base & Prima Scena
Ora farò una piccola e breve distinzione tra Scena e Nodo, la scena è
il dove andiamo a creare tutto quello che serve nel nostro gioco e i
nodi sono il mezzo per creare oggetti/entità dentro alla scena, per
creare confusione vi dirò che la scena in realtà è un nodo.
È più facile la pratica della teoria, quindi iniziamo:

Sotto al tab *Scene* andiamo a selezionare, 2D Scene ora li abbiamo un
nodo che si chiamo "Node2D", bene lo rinominiamo, la convenzione è
quello di dare il nome Main o World, ma possiamo chiamarlo anche Pippo
o Topolino. Per rinominare ci basta cliccare due volte su di esso e ora
possiamo già salvare questa Scena, ma prima impariamo a fare
ordine. Nel Tab FileSystem con il tasto destro creiamo una cartella, io
di solito la chiamo src, e ora possiamo fare C-S per salvare la
scena, selezioniamo src e salviamo con il nome Main.tscn.
Ora abbiamo una scena vuota con un nodo generico, ci serve un player e
un oggetto, creiamolo per iniziare, premendo il '+' che si trova nel
tab delle scene o C-a possiamo aggiungere un nodo, cerchiamo
il nodo sprite e aggiungiamo il nodo come figlio di main. Trasciniamo
il file "icon.png" nel riquadro texture del nodo sprite. Ora abbiamo
un oggetto che ci servirà come punto ti riferimento per il nostro
player.

![](dragdrop.png)

## Collisionshape
Ora salviamo e prepariamoci per creare il player, per farlo ci servirà
una nuova scena, quindi sul '+' in alto al centro, creiamo una nuova
scena e scegliamo *other node*. Cerchiamo il nodo "KinematicBody2D" e
lo rinominiamo con il nome "Player". Questo nodo ha bisogno di un
altro nodo minimo, se siete dei buoni osservatori avrete notato il
segnale di pericolo in giallo vicino al nodo Player, per chi non avesse
notato il segnale è fortunato ad avere un bravo insegnante come me, se
andate sul segnale con il mouse vi dirà "Warning: this node has no
shape....", ogni nodo con il nome Body ha bisogno di una shape, quindi
C-a e cerchiamo e aggiungiamo il nodo *CollisionShape2D*. Ora abbiamo
un nuovo segnale di pericolo, questo perchè la *Collisionshape2d* non
ha una forma, per dare una forma alla *Collisionshape2d* ci basta
andare sull'Inspector nella voce *Shape* e scegliere la forma, questo
si fa in base alla forma della sprite, noi scegliamo la
*RectangleShape2D*. 

![](shape2.png)

## Sprite e Texture
Ora ci serve una sprite per il nostro Player, di
nuovo facciamo C-a e cerchiamo *Sprite* e trasciniamo "icon.png"
dentro a texture come abbiamo fatto in precedenza. 
Per distinguere il Player con l'oggetto fatto poco fa, cambiamo il
colore alla foto, selezionando la sprite del player e scorrendo in
basso nell'ispector troviamo il menu "Visibility" e selezioniamo
"modulate", scegliamo il colore che ci aggrada. Prima di passare al
capitolo finale vi faccio fare ancora una cosa, selezionamo il nodo
"Player" e nel menu centrale in mezzo al lucchetto e all'osso è
presente un simbolo non identificato (guarda la foto) lo premiamo,
questo permette di spostare il nodo dove ci pare e tenendo uniti i
figli del nodo.

![](dragme.png)

## Final Step Camera
Questo è l'ultimo step prima del codice, dobbiamo di nuovo fare C-a e
scegliere il nodo Camera2D da aggiungere al player. Nell'ispector
della camera 2D bisogna abilitare *current*, dovreste vedere il
rettangolo della camera. Una volta fatto abbiamo
finito il player e possiamo salvare (C-s), e salviamo la scena con il
nome "player.tcsn".

![](camera.png)

## Script
Prima d'iniziare gli amanti dei tipi devono sapere che di default
l'auto completamento non mette i tipi alle funzioni, si sistema tutto
andando sul menu Editor->Text Editor->Completion e abilitare Add Type
Hints.

Ora selezioniamo il player, in alto a destra esiste una pergamena
clicchiamo sulla pergamena, si apre il menu e voglio darvi un *tip*,
in template possiamo scegliere diverse opzioni tra "Default", 'No
"comment" e "empty", oggi lasciamo "Default" ma in futuro vi consiglio
"No comment" o "empty" perchè vi fa risparmiare tempo, chiamiamo lo
script con il nome "Player.gd" e facciamo *create*. 
Lo script si presenta cosi:

```gdscript
extends KinematicBody2D
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
```

Scriviamo la parte di codice che ci servirà per far si che il player
si muova, partiamo dichiarando una variabile globale speed, che ci
servirà più tardi.
Andiamo a lavorare tramite la funzione di godot *_physics_process*,
questa funzione viene chiamata 60 volte al secondo e serve a gestire
tutti gli elementi fisici, in questa funzione ci servira una variabile
move *Vector2*, che servirà per impostare la direzione del player. 

```gdscript
extends KinematicBody2d

const SPEED = 150

func _physics_process(delta: float) -> void:
	var move := Vector2()

	if Input.is_action_pressed("ui_up"):
		move.y -= 1
	if Input.is_action_pressed("ui_down"):
		move.y += 1
	if Input.is_action_pressed("ui_left"):
		move.x -= 1
	if Input.is_action_pressed("ui_right"):
		move.x += 1
		
	move.normalized()
```

Non basta ancora, per muovere il player serve una funzione che sia in
grado di applicare una forza per muoverlo. Ci sono tante funzioni in
Godot per muovere il player, *move_and_slide* è quello che fa per noi.

```gdscript
extends KinematicBody2d

const SPEED = 150

func _physics_process(delta: float) -> void:
	var move := Vector2()

	if Input.is_action_pressed("ui_up"):
		move.y -= 1
	if Input.is_action_pressed("ui_down"):
		move.y += 1
	if Input.is_action_pressed("ui_left"):
		move.x -= 1
	if Input.is_action_pressed("ui_right"):
		move.x += 1
		
	move.normalized()
	move_and_slide(move * SPEED) 
	
```

Ci sono ancora delle cose spiegare, tutti quei "ui_qualcosa" si
possono modificare in project->project settings->Input Map.
Vi basti sapere che i tasti per muoversi sono le 4 frecce (Su, Giù,
Destra, Sinistra). Ma per questo potrei fare un tutorial in futuro.

Ma ora voglio finire il tutorial, C-s per salvare lo script.
Ora inseriamo il player nella scena *Main* e proviamo se funziona.
Apriamo la scena main e clicchiamo sulla catena in alto a destra,
vicino al tab "Scene" e selezionate la scena player.

![](instance.png)

Ora possiamo premere il tasto play in alto e scegliere la scena
"main" e vedere che il palyer si muove!
Grazie di aver letto fino a qui, siamo arrivati alla fine di questo
tutorial introduttivo a Godot, in futuro farò un tutorial sul movimento
migliorato.

Firmato Marrix

