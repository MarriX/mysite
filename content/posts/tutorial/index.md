---
title: "Come fare un tutorial"
weight: 4
date: 2021-02-23
---
# Avvertenze

1. Avvertenza: questo post è ironico, ma può risultare utile 
2. Avvertenza: in questo post ci sono software presenti su Linux o BSD,
perché non so cosa consigliare per utenti Windows o Mac osx, essendo un
post ironico non sono consigli veri.

Buon divertimento!

# Come si fanno i tutorial?

Lo so, nessuno ci ha mai pensato, ma secondo me ci vuole un tutorial
per fare i tutorial, e visto che io non ho avuto nessuno che mi
insegnasse a fare tutorial sul web ho pensato di prendermi questa
responsabilità, mamma mia che fortunelli. 

## Il linguaggio

Il linguaggio è importante, che voi scriviate o che filmiate cercate di
parlare al plurale o di dare ordini, per esempio "facciamo in questo
modo" o "fai in questa maniera". Il primo approccio è quello più
corretto, il "facciamo" fa riferimento a me e te, gli fai vedere che
anche tu hai dovuto compiere gli stessi passaggi, invece dire "fai" diventa un
ordine e forse spaventa, nessuno vuole sentirsi dare
ordini. Ricordatevi il vostro tutorial sarà visto da perfetti
ignoranti, incapaci, senza di voi sono delle pecore smarrite,
esattamente come voi in questo momento,siate gentili con il pubblico
e non insultate mai, anche se non avete cattive intenzioni e volete
solo fare ironia, non tutti capiscono l'ironia, ma sopratutto i
tutorial sono fatti per essere noiosi.

## Software

Che si tratti di video o di post su un sito puzzolente, abbiamo
bisogno di software, nel caso del video un video editor, qui vi posso
consigliare solo 2 software [Olive Video
Editor](https://www.olivevideoeditor.org/) o
[Shotcut](https://shotcut.org/), siccome non è il mio campo di
competenza non so cosa vi consiglio, quindi se questi video editor non
sono buoni, cazzi vostri!
Il mio campo è quello del blog, quello che vi serve in questo caso è
un software per gli screenshot e un editor di testo. Il software per
gli screen che vi consiglio è uno script fatto in casa.

```sh
#!/bin/sh


if [ "$1" = "rect" ]; then
	maim -s ~/screenshots/$(date +%s).png
else
	maim  ~/screenshots/$(date +%s).png
fi

notify-send "Screenshot yeah"

```

Salvate questo file dove vi pare e inserite dei tasti nella config del
vostro wm preferito per poter fare screen rapidi. Ricordatevi di fare
`doas pkg_add maim` se no non funziona (sostituire pkg_add con il
vostro gestore pacchetti e doas con sudo nel caso usiate Linux). Una
volta fatto uno screenshot vi tocca editarlo e aggiungere elementi per
indicare o mettere in evidenza, quindi vi tocca usare un editor come
[GIMP](https://www.gimp.org/). Ma io sono un profesionista, una
persona competente e vi consiglio un software che vi farà editare gli
screenshot subito dopo averli fatti, il nome di questo software è 

## Conclusione 

Grazie di aver letto questo tutorial, spero di avervi aiutato e di
avervi fatto divertire, un saluto MarriX.

## Software II

Dicevo il nome di questo software è
[Ksnip](https://github.com/ksnip/ksnip) questo software è molto
utile per chi volesse fare tutorial con tanti screenshot, l'ho usato
per il mio ultimo
[tutorial](https://riccardomazzurco.com/posts/group/).
Per finire vi servirà un editor di testo, non esiste altro editor
al di fuori di [Emacs](https://www.gnu.org/software/emacs/).
Di editor ne esistono a centinaia, sta a voi trovare quello che fa per
voi, io uso emacs e ve lo consiglio.

## Conclusione II

Grazie di aver letto questo post diverso dal solito. questo tutorial è
ironico, era da tanto che non pubblicavo qualcosa e volevo variare
argomento e ho deciso di sperimentare con un tutorial fake. Spero sia
stato utile e divertente, ci sentiamo al prossimo tutorial/post.

Saluti MarriX!


