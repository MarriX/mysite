---
title: "Godot Groups"
weight: 5
date: 2021-02-07
---

## I gruppi di Godot

Parto con il dire che questo non è un tutorial, non conosco la teoria
ma solo la pratica, ovvero non sarà qualcosa di tecnico, ma solo
qualcosa che ho imparato a tentativi di errori e nulla di troppo
complesso, ma qualcosa di veramente utile!
Che roba è un gruppo in Godot? I gruppi sono un insieme di nodi alla
quale si associa un nome. Possiamo creare una serie di nodi
*KinematicBody* e inserirli nel gruppo "Zombies" o nel gruppo "npc",
questo ci permetterà di interagire con tutti i nodi in quel gruppo
contemporaneamente. Ci sono tre funzioni per interagire con i gruppi
**call_group()**, **is_in_group()** e **add_group()**.

#### Creare un gruppo

La prima cosa da fare e creare un gruppo, come si crea un gruppo?
Esistono due modi, tramite codice o in maniera diretta con
l'interfaccia di Godot. Vediamo prima come si fa a creare un gruppo
tramite codice, usiamo la funzione *add_group(String)*, questa
funzione richiede un paramentro stringa che corrisponde al nome del
gruppo, vi consiglio di usarla all'interno della ready del nodo in
questione in questo modo:

```gdscript
extends KinematicBody

func _ready() -> void:
	add_to_group("NPC")
```

In questo modo abbiamo creato un gruppo, nello specifico abbiamo un
classico nodo *KinematicBody* e nella _ready ci ritroviamo la chiamata
a funzione add_to_group("NPC") che aggiunge il nodo al gruppo di nome
"NPC". Per creare un gruppo senza usare il codice bisogna fare in
questo modo: 

![](group.png)

Selezioniamo il nodo alla quale vogliamo aggiungere il gruppo, nel mio
esempio è un nodo *Kinematicbody* alle quale assegnamo il gruppo
**NPC**, scegliamo il tab *Node* vicino all'Inspector poi la voce
*Groups*, scriviamo il nome del gruppo e facciamo *Add*, ora al nostro
nodo sarà spuntato un simbolo nuovo, ci fa capire che questo nodo
appartiene ad un gruppo!

![](group2.png)

#### Come sfruttare i gruppi

Ora che abbiamo creato dei gruppi per distinguere i nodi, come si
possono sfruttare? Ci sono alcune situazioni per sfruttare i gruppi,
vi farò degli esempi e cercherò di spiegare come sfruttare le
altre funzioni che riguardano i gruppi. Per esempio vogliamo passare
agli zombie la posizione degli npc, in modo che gli zombie siano in
grado di inseguire gli npc continuamente senza sosta. Vediamo come si
fa: 

```gdscript
extends KinematicBody #npc 

func _ready() -> void:
	get_tree().call_group("zombies", "set_npc", self)
```

Questa è la seconda di tre funzioni per i gruppi, *call_group* nel
nostro esempio chiama *set_npc* con la variabile *self* all'interno
del gruppo zombies. Quindi nell'ipotetico file "zombies.gd" bisogna
creare una funzione *set_npc* che verrà chiamata una volta che il
nostro npc è stato caricato nella scena. Vi faccio vedere anche come
va impostato il codice per gli zombies.

```gdscript
extends Kinematicbody #zombies

var npc = null

func _ready() -> void:
	add_group("zombies")

func set_npc(n):
	npc = n 
	
```

Ora all'interno della variabile npc abbiamo a tutti gli effetti
il nostro npc. Perché quando nell'esempio precedente chiamiamo la
funzione *call_group*, passiamo la variabile self e di conseguenza in
*set_npc* la variabile n è il nostro npc. Se facciamo, magari
all'interno di una *process*, un print "(npc.position)" ci verrà stampata
la posizione del *npc*. Questo utilizzo di *call_group* ci permette di
ottenere qualsiasi variabile o funzione dei nostri npc, non solo la
posizione, ma possiamo anche ottenere una qualsiasi variabile globale
che abbiamo dichiarato all'interno del codice del nostro npc. Ora ci
manca da spiegare solo la funzione **is_in_group**, semplicemente è
una funzione booleana, torna vero se un certo nodo fa parte di un
gruppo e invece torna falso se non ne fa parte. Un esempio di
utilizzo di questa funzione è se abbiamo una serie di oggetti e
vogliamo sapere il tipo di oggetto dentro alla nostra area. Per sapere
se qualcosa è entrata nell'aera ci basta usare il segnale *body
entered*, se connettiamo questo segnale ci troviamo qualcosa del
genere:

```gdscript
extends Kinematicbody

func _on_npc_body_entered(body: Node) -> void:
	pass
```

Per sapere che tipo di oggetto è dentro l'area ci basta fare
*print(body.name)*, e se vogliamo interagire solo quando un oggetto
specifico è dentro l'area possiamo fare *if body.name == "nome oggetto"*.
Ma se noi abbiamo un oggetto generico che si chiama "@oggetto01", e la
volta dopo si chiama "@oggetto00", come facciamo? Ipotizziamo di avere
una variabile *id* che sovrascrive il nome dell'oggetto. Ci basta fare
*if body.id == "nome oggetto"*, ma se dentro l'area esiste un oggetto
senza la variabile *id* siamo fregati. Ma ecco la soluzione:

```gdscript
extends Kinematicbody

func _on_npc_body_entered(body: Node) -> void:
	if body.is_in_group("items"):
		if body.id == "nome oggetto":
			modifica_oggetto(body)

```

Sfruttiamo *is_in_group("items")* per capire se si tratta di un
oggetto all'interno del gruppo *items* e poi ci chiediamo se l'id
corrisponde a un oggetto specifico, modifica_oggetto è pura fantasia.
Questa è l'ultima funzione per i gruppi, magari l'esempio non è dei
migliori, però spero di aver reso l'idea di come funzioni.

#### Conclusioni

In conclusione i gruppi sono una maniera di creare un'insieme di nodi,
e in certe situazioni sono utili per ottenere dati, variabili o
funzioni di un nodo particolare senza usare segnali, che a volte sono
complessi da usare. I gruppi sono semplici, con poche funzioni, ma
bisogna valutare bene se usarli. Spero che questo tutorial vi sia
stato utile, un saluto Marrix. 
