---
weight: 6
title: "QuickInvetory e Focus"
date: 2021-01-14
---

## QuickInvetory

Che roba è un Quickinvetory? È un inventario rapido tipo quello che
hanno Minecraft in basso e Terraria in alto. Volevo realizzare un
inventario simile per un gioco che sto creando con il nostro amico
Godot Engine. Le difficoltà sono venute fuori quando volevo creare il
focus, ovvero il contorno che evidenza quale slot dell'inventario è
stato selezionato. Con questo tutorial non vi spiegherò codice, ma
solo un metodo per avere il focus degli slots.

#### Impostazioni Base

Andiamo nel menù a tendina *Project* e scegliamo *Project Settings*,
andiamo a impostare la risoluzione del nostro monitor, io
personalmente in *Height* ho messo 1920 e in *Width*, ora scorrendo in
basso troviamo la voce *Mode* scegliamo l'opzione *2D* e in *Aspect*
selezioniamo la voce *expand* come nella foto.

![Project Settings](risoluzione.png)

#### Inventario Base

Ora vi faccio vedere come fare un inventario base, ma dove non funzionerà
il focus, lo faccio di quattro slots per darvi un idea base.
In un progetto già esistente creiamo una scena con il nodo *Control*
come padre, e aggiungiamo come figlio il CenterContainer e selezioniamo
*Layout Full Rect* e creiamo un *MarginContainer*. Nell'ispector del
MarginContainer modifichiamo la *Min Size* in questa maniera x a 300 e
y a 1080 (stessa altezza del vostro monitor), ora vi ricordo che le
misure si basano sul fatto che il mio inventario sarà di quattro
slots, sta a voi vedere quanto largo nel caso di più slots. Per ultimo
anche se non ultimo, in basso nell'ispector in *Custom Constants*
scegliamo *Margin top* lo mettiamo a 1000, ora creiamo un altro
*MarginContainer* come figlio del primo, rinominiamo il nodo Contro in
HUD e salviamo con C-s in HUD.tscn.

![HUD](hud.png)

Ora creiamo una nuova scena nuova e scegliamo *other node* e cerchiamo
*HBoxContainer*, rinominiamo come *QuickInvetory* e come figlio
agiugiamo un *VBoxContainer*, modifichiamo nell'ispector di entrambi i
nodi la *Min Size* a 300 x e 70 y per il nodo *QuickInvetory* e 70 x e
70 y per il nodo *VBoxcontainer*, ora aggiungiamo un nodo *Panel* e
impostiamo la min size a 70 x e 70 y, come figlio del *Panel*
aggiungiamo un nodo *TextureRect* e facciamo *Layout Full Rect*. Ora
per finire, tasto destro sul *VBoxcontainer* e scegliamo duplicate
fino a quando non abbiamo quattro slots, salviamo la scena come
QuickInvetory.tscn. Una volta fatto tutto ciò, selezioniamo tutti i
nodi *Texturerect* e nel menù *Focus* impostiamo *Mode All*.

![focus](all.png)

#### TexutureButton

Ora selezioniamo il nodo *QuickInvetory* e creiamo uno script, ci
servirà solo una riga di codice, dobbiamo chiamare il focus per il
nodo Texturerect.

```gdscript
extends HBoxContainer

func _ready() -> void:
	$VBoxContainer/Panel/TextureRect.grab_focus()

```

Ed ora possiamo far partire la scena con F6, e se usiamo le frecce
destra e sinistra notiamo che non succede nulla, perché? Beh il focus
automatico funziona solo con i nodi *button*, ma esiste un nodo che ci
eviterà problemi. Ecco che ora inizia il vero tutorial, ovvero la
parte importante, selezioniamo tutti i nodi *Texturerect* e con il
tasto destro scegliamo *change type* e cerchiamo il nodo
*TextureButton* e clicchiamo su change. Ora ci servirà una *sprite*
per il focus e una *sprite* per il test. La *sprite* per il focus deve
essere un riquadro contornato con il vuoto al centro, per la *sprite*
test usiamo l'icona di Godot. Selezioniamo tutti i nodi *TextureRect*
e nell'ispector mettiamo in *Normal* l'icona di Godot e in *Focus* la
sprite contornata e abbiamo fatto.

![Button](button.png)

Possiamo avviare la scena come abbiamo fatto poco fa e ora se usiamo
le frecce destra e sinistra il focus si sposta da uno slot all'altro.
Finiamo la nostra opera aprendo la scena HUD.tscn e selezioniamo il
nodo *MarginContainer* e con il tasto destro scegliamo *Instance
Child Scene* e scegliamo la scena QuickInvetory.tscn ed ora abbiamo un
inventario rapido in stile Minecraft, sta voi ora ad implementare
tutte le funzioni che vi servono, anche se in futuro vi farò qualche
tutorial, visto che senza di me siete persi. A parte le battute direi
di tirare le somme di questo tutorial.

#### Conclusione 

Per finire il focus è in automatico, per avere il focus su un nodo di
tipo *Control* basta chiamare la funzione grab_focus() e impostare
*Focus Mode All* e focus sarà presente, il problema è che non sarà
visibile per i nodi che non sono *Button*, quindi vi consiglio di
usare le *TextureButton* se vi serve una *sprite* più un *focus* e di
usare *TextureRect* o semplicemente il nodo *Sprite* nei casi cui non
vi serve un focus visibile. Per finire se volete un focus del tipo 1 2
1, ovvero un focus che con un tasto soltanto ti permette di fare il
giro completo, vi basta mettere nell'ispector dell'ultimo
*Texturebutton* sotto la voce *Neighbour Right* il primo
*Texturebutton*.

Spero che questo tutorial vi sia stato d'aiuto e vi consiglio di
guardare il mio vecchio tutorial su Godot, e di leggere anche tutti
gli altri articoli, saluti Marrix.

