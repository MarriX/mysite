---
weight: 7
title: "Alternativa a..."
date: 2021-01-01
---
## Open Source o Free Software
Il mondo del software è diviso in 2, esiste il software *libero/Open
Source* e poi esiste il *Close Source* o *software proprietario*. Non
voglio parlare di tutte le differenze, posso dirvi che la diversità
principale tra close e open source è nell'uso della licenza.

Le licenze *Open Source* permettono di condividere e modificare il
codice di un determinato *software* e sono più liberi nella
distribuzione di esso.
Le licenze *Close Soure* non permettono agli utenti di distribuire e
modificare il codice. 

#### Software che non ti aspetti

In questo post vi darò una lista alternativa al solito Software *Close
Source*:

1. **Libre Office** alternativa a **Microsoft Office**:

Io nella mia Vita ho spesso avuto a che fare con il software della
**Microsoft**, ma poi quando mi sono reso conto che avrei dovuto
pagarlo da solo ho pensato "M***a! Costa tanto! Ma per scuola mi
serve!", avevo due scelte scaricarlo illegalmente o trovare
un'alternativa valida e l'ho trovato in **Libre Office**.
In questo momento penso che **Libre Office** sia migliore in tutto di
**Microsoft Office**, ma la gente ha la mentalità che non la porta a
usarlo, secondo me pensa "Gratis = non professionale", Libre Office
dimostra che un Software gratis è professionale quanto uno non gratis.

2. **GIMP** alternativa ad **Adobe Photoshop**:

Io non ho mai usato **Adobe Photoshop** e non sono un fotografo, ma
per alcuni lavori ho avuto la necessita di usare qualcosa di più grande
di un Tux Paint (che non sarà in questa lista), è perfetto per i non
professionisti e lo dico solo perché non so come sia Photoshop. Non ho
i dati per dire quale dei due software è più professionale, Ma
sopratutto è perfetto per chi ha bisogno di fare certi lavori senza
dover pagare cifre assurde. Quindi consiglio GIMP per chi vuole
lavorare ai propri progetti con un software leggero e gratuito.

3. **Godot** alternativa a **Unity3D**:

Questa scelta è quella che dovete seguire più di ogni altra, conosco
entrambi i *Game Engine*, ma solo Godot mi ha dato soddisfazione
nell'essere usato. Lo trovo veramente il miglior Game Engine con la
quale partire, intuitivo, gratuito e Open Source, la community è
veramente disponibile e in crescita, in questo sito troverete il
miglior tutorial per iniziare con [*Godot*](https://riccardomazzurco.com/posts/hellogodot/). 
Piccola Curiosità su Godot il nome di questo software è preso dal nome
del protagonista dell'opera teatrale "Waiting for Godot".

4. **Olive Video Editor** alternativa ad **Adobe Premiere**:

Questa scelta è una di quelle che faccio senza i dati, non ho provato
nessuna delle due, ma questo è il mio blog, il più strano che puoi
trovare al mondo. Ma tenete d'occhio questo **Video Editor** in
futuro potrebbe rubare molto alla concorrenza, più in avanti lo proverò
e vi farò sapere, ma per il momento è una [scommessa](https://www.olivevideoeditor.org/).

5. **Emacs** alternativa a qualsiasi software al mondo:

Emulatore Terminale, *Window Manager*, **Telgram Desktop** e tanto alto,
dimenticavo è anche un editor di testo, sto usando Emacs in questo
momento  per scrivere questa pagina.
Lo puoi usare per scrivere in qualsiasi linguaggio e sopratutto ha un
suo linguaggio di programmazione (emacs lisp), quindi ha la
possibilità  di essere esteso all'infinito. Difetto di Emacs non
esiste un Kernel per questo sistema Operativo.

#### Conclusione 

Non esiste una conclusione per questa lista, ci sarà una parte 2 di
questo post con altre 5 Alternative a software *close source*, spero
di avervi fatto conoscere dell'ottimo software e di avervi aperto la
mente sul mondo dell'Open Source.
Concludo con il dire che sono stato meschino con questo post e di non
aver usato i termini appropriati come *Free Software* o *Software
Proprietario*, ma lo fatto a scopo di comunicare meglio il messaggio.

Detto Questo un saluto e un ringraziamento dal vostro Marrix. 
