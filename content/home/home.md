---
menu:
  main:
    weight: 1
title: "Welcome"
---
Benvenuto, sono Riccardo, in questo sito troverai tutto sulla mia
persona, ti invito a dare un occhiata alle varie pagine e ai vari link
per contattarmi. \ 
Sono nato il 29/06/1994,ho un diploma in ragioneria (dimostrazione 1+1
= 2) e ho frequentato Informatica all'università (dimostrazione 1+1 = 10).\ 
Porto nel cuore l'informatica e vorrei tanto fare la mia parte, non
ambisco a arrivare ad essere il nuovo Linus Torvalds o Theo de Raadt
mi basta diventare bravo come Steve Ballmer (con meno sudore e più
capelli). Ma il mio vero scopo è quello di sfruttare il mezzo del web
per condividere le mie conoscenze, e i vari *tips and tricks* che
potrai imparare durante il mio percorso, come direbbe un noto
developer, artista e cantante "Join us now and share the software". 

