---
menu:
  main:
    weight: 2
title: "Skills"
---
Cosa posso dare alla tua Azienda?

Conscenze Informatiche di base:
* Suite Office o LibreOffice
* Latex
* Sistema Operativo Windows 
* Sistema Operativo Mac Osx
* Sistema Operativo Linux/BSD

Conoscenze Web:
* MarkDown
* Hugo

Conoscenza Informatica Avanzata:
* Linguaggio di Programmazione C
* Linguaggio di Programmazione Go
* Linguaggio di Programmazione Java
* Linguaggio di Programmazione Common Lisp
* Linguaggio di Scripting Bash
* Esperienza lavorativa con Godot Engine
* Linguaggio di Scripting gdscript

TODO:
* Imparare il Linguaggio Crystal
* Imparare il Linguaggio Python
* Imparare il Linguaggio PHP
