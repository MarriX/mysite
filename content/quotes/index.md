---
menu:
  main:
    weight: 6
title: "Quotes"
---

## Le migliori citazioni dei migliori *Developers*

Ecco qui una lista di frasi dei migliori *Developers*, vi ricordo che
questa è una classifica personale, partirò dall'ultima posizione:

10. Facebook Is a Surveillance Engine, Not Friend (Richard Stallman
2012).

Questa frase è molto pungente, ma è la pura verità: Facebook è un motore
di sorveglianza, non un amico. Questa frase non è contro Facebook, ma
contro ai siti che sorvegliano tutto ciò che fai, Facebook è il
colosso.

9. No problem is too big it can't be run away from (Linus Torvald).

Questa citazione è profonda, stupenda, geniale, anche se non ho
capito il suo significato.

8. Only failure makes us experts (Theo de raadt).

Questa è molto semplice ed è utile per tutti i tipi di mestieri.

7. It's my personality naturally to try to fly under the radar (Erik
   Spoelstra).
   
Io già sento i commenti "eh ma Erik Spoelstra non è un developer",
poveri ignoranti. 

6.  This software is free, so on the count of three, update to six
    point eight! (Hacker People OpenBSD song)

Questa è la prima strofa della canzone "Hacker People", canzone da
ascoltare se sei un vero developer.

5. Nvidia FUCK YOU (Linus Torvalds).

Parole molto profonde del developer di Linux nei confronti
dell'azienda produttrice di schede grafiche.

![Linus Torvalds](nvidia.jpg) 

4. C is quirky, flawed, and an enormous success (Dennis Ritchie).

C è bizzarro, imperfetto e un enorme successo. Comunque un linguaggio
migliore di C++.

3. Idiots can be defeated but they never admit it (Ricchard Stallman).

Sinceramente io non sono mai stato sconfitto, tranne tutte le volte
che ho tentato.

2. Intelligence is the ability to avoid doing work, yet getting the
   work done (Linus Torvalds).

Nulla di più vero, e ora siete pronti per la più grande citazione
nella storia?

Ecco a voi la numero 1:

1. Mi sono reso conto che un fottuto trattino basso ti cambia la vita
   (Riccardo Mazzurco 2020).
   
Beh che dire tenetevi pronti per altre grandi citazioni in futuro.

Saluti MarriX
